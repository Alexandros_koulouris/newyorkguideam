﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V7.App;
using SupportFragment = Android.App.Fragment;
using System.Collections.Generic;
using NewYorkGuideAM.Fragments;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;

using System;
using Android.Views;
using Android.Content;
using Android.Webkit;
using Android.Runtime;
using Android.Support.V7.Widget;
using Firebase.Xamarin.Database;
using NewYorkGuideAM.Model;

namespace NewYorkGuideAM
{

    [Activity(Label = "GridLayout_CardView", MainLauncher = true, Theme = "@style/Theme.AppCompat.Light.NoActionBar")]
    public class MainActivity : AppCompatActivity, Android.Support.V7.Widget.SearchView.IOnQueryTextListener
    {
        // Theme = "@style/Theme.AppCompat.Light.NoActionBar"
        public SupportToolbar mToolbar;
        private FrameLayout mFragmentContainer; 
        private MainFragment mFragment1;
        private BarsFragment mBarsFragment;
        private SupportFragment mCurrentFragment;
        private Stack<SupportFragment> mStackFragments;
        private const string FirebaseURL = "https://newyorkguideam.firebaseio.com/";

        protected async override void OnCreate(Bundle savedInstanceState)
        {

            
            base.OnCreate(savedInstanceState);
            
            // Set our view from the "main" layout resource

            SetContentView(Resource.Layout.Main);

            await Loaddata();
            


            var mFragmentContainer = FindViewById<FrameLayout>(Resource.Id.fragmentContainer);
          
            mFragment1 = new MainFragment();
             mBarsFragment = new BarsFragment();

             mStackFragments = new Stack<SupportFragment>();

           // var trans = SupportFragmentManager.BeginTransaction();
            FragmentTransaction fragmentTx = FragmentManager.BeginTransaction();
            fragmentTx.Add(Resource.Id.fragmentContainer, mFragment1, "MainFragment");
            fragmentTx.Commit();


            fragmentTx.Add(Resource.Id.fragmentContainer, mBarsFragment, "BarsFragment");
            fragmentTx.Hide(mBarsFragment);



            mCurrentFragment = mFragment1;
            //mToolbar = FindViewById<SupportToolbar>(Resource.Id.toolbar);
           // SetSupportActionBar(mToolbar);
           // mToolbar.Visibility = Android.Views.ViewStates.Visible;


            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            toolbar.SetTitleTextColor(Android.Graphics.Color.Black);
            //toolbar.SetNavigationIcon(Resource.Drawable.logo);
            SetSupportActionBar(toolbar);

        }

        

        public  bool OnCreateOptionsMenu(IMenu menu)
        {
            var inflator = MenuInflater;
            inflator.Inflate(Resource.Menu.ToolBarMenu, menu);
            var searchItem = menu.FindItem(Resource.Id.search);
            searchItem.SetVisible(false);
            //SearchView searchView = (SearchView)menu.FindItem(Resource.Id.search.getActionView();
            var item = Android.Support.V4.View.MenuItemCompat.GetActionView(searchItem);
            Android.Support.V7.Widget.SearchView searchView = item.JavaCast<Android.Support.V7.Widget.SearchView>();
            // searchView.SetOnQueryTextListener(this);

            //SearchManager searchManager = (SearchManager)GetSystemService(Context.SearchService);
            // SearchView searchView = (SearchView)menu.FindItem(Resource.Id.search);
            //searchView.SetSearchableInfo(searchManager.GetSearchableInfo(getComponentName()));

            searchView.QueryTextSubmit += (sender, args) =>
            {
                Toast.MakeText(this, "You searched:sublmit " + args.Query, ToastLength.Short).Show();

            };

            searchView.QueryTextChange += (sender, args) =>
            {
                //List<String> filteredModelList = SimpleRecycler.MyAdapter.updateData(galaxies, args.NewText);
                //SimpleRecycler.MyAdapter.setItems(filteredModelList);
                //SimpleRecycler.MyAdapter.notifyDataSetChanged();
               // Bundle bundle = new Bundle();
               // bundle.PutString( args.NewText, "From Activity");
                // set Fragmentclass Arguments
                //Fragmentclass fragobj = new Fragmentclass();
               // fragobj.setArguments(bundle);

                Toast.MakeText(this, "You searched:change "+args.NewText , ToastLength.Short).Show();

            };


            return true;
            //return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.search)
            {
                Toast.MakeText(this, " Do Search", ToastLength.Short).Show();
               // SupportInvalidateOptionsMenu();
                return true;
            }
            else if (id == Resource.Id.share)
            {
                Toast.MakeText(this, " Share what ever you want", ToastLength.Short).Show();
                return true;
            }
            else if (id == Resource.Id.email)
            {
                Toast.MakeText(this, " Send Mail", ToastLength.Short).Show();
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        public bool OnQueryTextChange(string newText)
        {
            return true;
        }

        public bool OnQueryTextSubmit(string query)
        {
            return false;
        }

        private async System.Threading.Tasks.Task Loaddata()
        {
            var firebase = new FirebaseClient(FirebaseURL);
            var items = await firebase.Child("users").OnceAsync<Bar>();
            foreach (var item in items)
            {
                Bar bar = new Bar();
                 
                 bar.Name = item.Object.Name;
            }

        }
    }
}

