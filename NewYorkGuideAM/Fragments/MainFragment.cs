﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;



namespace NewYorkGuideAM.Fragments
{
    public class MainFragment : Fragment 
    {
        //Android.Support.V4.App.Fragment
           //public MainActivity a;
           //MainActivity casted = Activity as MainActivity;

           //((MainActivity)Activity).;

           MainActivity act = new MainActivity();

        public override void OnCreate(Bundle savedInstanceState)
        {

           
            base.OnCreate(savedInstanceState);
            
            //View view = act.FindViewById(Resource.Id.toolbar);
            // this.Activity.SetVisible().IsHidden=true;

            // Create your fragment here
        }

        public override void OnStart()
        {
           
            base.OnStart();
        }
        public override void OnPause()
        {
            base.OnPause();
        }

        public override void OnStop()
        {
            base.OnStop();
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            SetHasOptionsMenu(true);
            View view = inflater.Inflate(Resource.Layout.MainFragmentPage, container, false);
            
            ViewFlipper viewFlipper = view.FindViewById<ViewFlipper>(Resource.Id.flipperid);
            Android.Support.V7.Widget.GridLayout mainGrid = (Android.Support.V7.Widget.GridLayout)view.FindViewById(Resource.Id.mainGrid);
            //SupportToolbar mToolbar = view.FindViewById<SupportToolbar>(Resource.Id.toolbar);

            //mToolbar.Visibility = Android.Views.ViewStates.Visible;
            viewFlipper.StartFlipping();
            SetSingleEvent(mainGrid);
            //SupportToolbar mToolbar = act.FindViewById<SupportToolbar>(Resource.Id.toolbar);
            // mToolbar.Visibility = Android.Views.ViewStates.Gone;
            //act.mToolbar.Visibility = Android.Views.ViewStates.Gone;
            return view;
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            inflater.Inflate(Resource.Menu.ToolBarMenu, menu);
            var searchItem = menu.FindItem(Resource.Id.search);
            searchItem.SetVisible(false);

            base.OnCreateOptionsMenu(menu, inflater);
        }
        //public  void onResume()
        //{
        //    base.OnResume();
        //   // ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
        //}

        // public void onStop()
        //{
        //    base.OnStop();
        //    //((AppCompatActivity)getActivity()).getSupportActionBar().show();
        //}

        private void SetSingleEvent(Android.Support.V7.Widget.GridLayout mainGrid)
        {
            for (int i = 0; i < 4; i++)
            {

                CardView cardView = (CardView)mainGrid.GetChildAt(i);

                cardView.Click += delegate
                 {


                     Toast.MakeText(View.Context, "Position: ", ToastLength.Short).Show();
                    

                     Activity
                      .FragmentManager
                       .BeginTransaction()
                       .Replace(Resource.Id.fragmentContainer, new BarsFragment())
                       .AddToBackStack(null)
                        .Commit();
                     
                     //a.mToolbar.Visibility = ViewStates.Gone;
                     //cardView.SetBackgroundColor(Android.Graphics.Color.ParseColor("#00bfff"));
                     //System.Threading.Tasks.Task.Delay(2000);
                     //cardView.SetBackgroundColor(Android.Graphics.Color.ParseColor("#ffffff"));

                 };
            }
        }
    }
}