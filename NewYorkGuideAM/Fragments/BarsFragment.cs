﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using NewYorkGuideAM.Model;
using SimpleRecycler;

namespace NewYorkGuideAM.Fragments
{
    class BarsFragment: Fragment
    {
        //Android.Support.V4.App.Fragment
        private RecyclerView rv;
        private MyAdapter adapter;
        public MainActivity b;
        private const string FirebaseURL = "https://newyorkguideam.firebaseio.com/";
        JavaList<Bar> galaxies = new JavaList<Bar> { };
        
        //MainActivity activity = (MainActivity)getActivity();

        public  override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
           // await Loaddata();

            //b.mToolbar.Visibility = Android.Views.ViewStates.Visible;
            // Create your fragment here
        }

        public  override  View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            SetHasOptionsMenu(true);
            View view = inflater.Inflate(Resource.Layout.BarsFragmentPage, container, false);

            rv = view.FindViewById<RecyclerView>(Resource.Id.mRecylcerID);
            rv.SetLayoutManager(new LinearLayoutManager(view.Context));
            rv.SetItemAnimator(new DefaultItemAnimator());

           // new ReceiveBar(this, View).Execute();

            //adapter = new MyAdapter(getGalaxies(),view);
            //rv.SetAdapter(adapter);
            //adapter.NotifyDataSetChanged();

           
            
            return view;
        }

        public override async void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
            await Loaddata();

            adapter = new MyAdapter(galaxies, this.View);
            rv.SetAdapter(adapter);
            adapter.NotifyDataSetChanged();


            //Download the data from the REST API

        }


        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            inflater.Inflate(Resource.Menu.ToolBarMenu, menu);
            
            base.OnCreateOptionsMenu(menu, inflater);
            var searchItem = menu.FindItem(Resource.Id.search);
            var item = Android.Support.V4.View.MenuItemCompat.GetActionView(searchItem);
            Android.Support.V7.Widget.SearchView searchView = item.JavaCast<Android.Support.V7.Widget.SearchView>();

            searchView.QueryTextSubmit += (sender, args) =>
            {
                Toast.MakeText(View.Context, "You searched:sublmit " + args.Query, ToastLength.Short).Show();

            };

            searchView.QueryTextChange += (sender, args) =>
            {
                //List<String> filteredModelList = SimpleRecycler.MyAdapter.updateData(galaxies, args.NewText);
                //SimpleRecycler.MyAdapter.setItems(filteredModelList);
                //SimpleRecycler.MyAdapter.notifyDataSetChanged();
                // Bundle bundle = new Bundle();
                // bundle.PutString( args.NewText, "From Activity");
                // set Fragmentclass Arguments
                //Fragmentclass fragobj = new Fragmentclass();
                // fragobj.setArguments(bundle);

                Toast.MakeText(View.Context, "You searched:change " + args.NewText, ToastLength.Short).Show();

            };


        }


        //private class ReceiveBar : AsyncTask<string, Java.Lang.Void, string>
        //{
        //    private BarsFragment barsFragment;
        //    private View view;

        //    public ReceiveBar(BarsFragment barsFragment, View view)
        //    {
        //        this.barsFragment = barsFragment;
        //        this.view = view;
        //    }

        //    protected override string RunInBackground(params string[] @params)
        //    {

        //        var firebase = new Firebase.Xamarin.Database.FirebaseClient(FirebaseURL);
        //        var items = firebase.Child("users").OnceAsync<Bar>();

        //        //foreach (var item in items)
        //        //       {
        //        //      Bar bar = new Bar();

        //        //         bar.Name = item.Object.Name;
        //        //  }
        //        return "";

        //    }


        //    protected override void OnPostExecute( string items)
        //    {
        //        base.OnPostExecute(items);

        //       foreach (var item in items)
        //             {
        //            Bar bar = new Bar();

        //               // bar.Name = item.Object.Name;
        //            bar.Name = item.ToString();
        //         }

        //    }
        //}





        private async System.Threading.Tasks.Task Loaddata()
        {
            var firebase = new Firebase.Xamarin.Database.FirebaseClient(FirebaseURL);
            var items = await firebase.Child("users").OnceAsync<Bar>();
            foreach (var item in items)
            {
                Bar bar = new Bar();

                bar.Name = item.Object.Name;
                bar.Title = item.Object.Title;
                bar.PhotoURL = item.Object.PhotoURL;
                
                galaxies.Add(bar);
            }

        }




        private JavaList<string> getGalaxies()
        {

            JavaList<String> galaxies = new JavaList<String>
            {
                
                "The Top of the Rock Observation Deck is a top NYC attraction with the best views this winter, from Central Park to Manhattan's Midtown and Downtown skyscrapers",
                "Pinwheel",
                "IC 1011",
                "Sombrero",
                "Virgo Stellar Stream",
                "Canis Majos",
                "Cartwheel",
                "Ring Nebula",
                "Centaurus A",
                "Own Nebula",
                "Large Magellonic Cloud",
                "Small Magellonic Cloud",
                "MilkyWay",
                "Andromeda",
                "Messier 81",
                "Leo",
                "Hoag's Object",
                "Mayall's Object",
                "Messier 87",
                "Whirlpool",
                "Triangulumn"
            };

            return galaxies;
        }
    }
}