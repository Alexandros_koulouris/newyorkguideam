﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using NewYorkGuideAM;
using NewYorkGuideAM.Model;
using Android.App;
using NewYorkGuideAM.Interface;

namespace SimpleRecycler
{

    class MyAdapter : Android.Support.V7.Widget.RecyclerView.Adapter, IItemClickListener
    {
        private readonly JavaList<Bar> galaxies;
        private View recycleview;

        public MyAdapter(JavaList<Bar> galaxies, View view)
       // public MyAdapter(JavaList<Bar> galaxies)
        {
            this.galaxies = galaxies;
            this.recycleview = view;
        }

        //BIND DATA TO VIEWS
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            MyViewHolder h = holder as MyViewHolder;
            h.NameTxt.Text = galaxies[position].Name;
            h.Titletxt.Text = galaxies[position].Title;


            Square.Picasso.Picasso
    .With(recycleview.Context)
    .Load(galaxies[position].PhotoURL)
     .Resize(150, 200)
     .Placeholder(Resource.Drawable.placeholder)
     //.CenterCrop()
     .Into(h.photobar);
            h.SetItemClickListener(this);
        }

        //INITIALIZE VH
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View view = parent;

            //INFLATE LAYOUT TO VIEW
            //var inflater = recycleview.Context.GetSystemService(Context.LayoutInflaterService) as LayoutInflater;
           // view = inflater.Inflate(Resource.Layout.Model, null);

            View v = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.Model, parent, false);
            
            MyViewHolder holder = new MyViewHolder(v);

            return holder;
        }

        public void onClick(View itemview, int position, bool isLongClick)
        {
            if (isLongClick)
            {
                Toast.MakeText(recycleview.Context, "long click" + galaxies[position].Name, ToastLength.Short).Show();
            }
            else
            {
                Toast.MakeText(recycleview.Context, "click" + galaxies[position].Name, ToastLength.Short).Show();
            }
        }

        //TOTAL NUM OF GALAXIES
        public override int ItemCount
        {
            get { return galaxies.Size(); }
        }

        /*
         * Our Viewholder class.
         * Will hold our textview.
         */
        internal class MyViewHolder : RecyclerView.ViewHolder,View.IOnClickListener, View.IOnLongClickListener
        {
            public TextView NameTxt;
            public TextView Titletxt;
            public ImageView photobar;
            public IItemClickListener IitemCLickListener;


            public MyViewHolder(View itemView)
                : base(itemView)
            {
                NameTxt = itemView.FindViewById<TextView>(Resource.Id.nameTxt);
                Titletxt = itemView.FindViewById<TextView>(Resource.Id.headlineTxt);
                photobar = itemView.FindViewById<ImageView>(Resource.Id.photobar);
                itemView.SetOnClickListener(this);
                itemView.SetOnLongClickListener(this);
            }
            public void SetItemClickListener(IItemClickListener IitemCLickListener)
            {
                this.IitemCLickListener = IitemCLickListener;
            }

            public void OnClick(View v)
            {
                IitemCLickListener.onClick(v, AdapterPosition, false);
            }

            public bool OnLongClick(View v)
            {
                IitemCLickListener.onClick(v, AdapterPosition, true);
                return true;

            }
        }

        //public List<String> updateData(List<String> datas, String text)
        //{


        //    JavaList<String> newNames = new JavaList<String>;
        //    for (String name : datas)
        //    {

        //        if (name.contains(text))
        //        {
        //            newNames.Add(name);
        //        }
        //    }

        //    return newNames;
        //}

    }
}